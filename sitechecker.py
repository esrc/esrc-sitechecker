"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

try:
    from BeautifulSoup import BeautifulSoup as bs
except:
    pass
try:
    from bs4 import BeautifulSoup as bs
except:
    pass

import argparse
import datetime
import logging
import requests
import sys
import urlparse



class SiteChecker(object):
    """
    Enumerate accessible URLs on a public web site. Check accessible URLs against
    a list of known URLs.
    @see Based on https://github.com/sarang4/python-web-crawler/blob/master/web_crawler.py
    """

    def __init__(self):
        self.parser = argparse.ArgumentParser(description='Create an inventory of publicly accessible URLs, validate the accessibility of URLs at a specified web site.')
        self.parser.add_argument('--identify', help='Create an inventory of publicly accessible URLs at a specified web site. Write URLs to file1')
        self.parser.add_argument('--validate', help='Validate accessibility of URL paths specified in file1 against the specified site URL. Write URLs that fail to file2')
        self.parser.add_argument('file1', help='Data file')
        self.parser.add_argument('file2', help='Data file', nargs='?')
        self.log = logging.getLogger(__name__)

    def configure_logging(self):
        self.log.setLevel(logging.DEBUG)
        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        ch.setFormatter(formatter)
        self.log.addHandler(ch)

    def get_host_url(self, url):
        """
        Get the portion of the URL belonging to the host.
        """
        parsed = urlparse.urlparse(url)
        return "{0}://{1}".format(parsed.scheme, parsed.netloc)

    def identify(self, start_url, output):
        """
        Crawl web site. Report all URLs that succeed.
        """
        assert start_url, self.log.error("Starting URL not specified")
        assert output, self.log.error("Output file not specified")
        base = self.get_host_url(start_url)
        links_crawled = set()
        links_to_crawl = set()
        try:
            links_to_crawl.add(start_url)
            while len(links_to_crawl) > 0:
                try:
                    crawling_url = links_to_crawl.pop()
                    self.log.info("Visiting: {0}".format(crawling_url))
                    r = requests.get(crawling_url)
                    links_crawled.add(crawling_url)
                    # if the content type is text/html then extract outbound
                    # links to visit
                    if 'text/html' in r.headers['content-type']:
                        soup = bs(r.content)
                        parsed_url = urlparse.urlparse(crawling_url)
                        for a_tag in soup.findAll('a'):
                            link = a_tag.get('href')
                            if link and not link.startswith('#'):
                                if link.startswith('/'):
                                    link = parsed_url.scheme + parsed_url.netloc + link
                                elif link.startswith('..'):
                                    link = urlparse.urljoin(crawling_url, link)
                                else:
                                    link = urlparse.urljoin(base, link)
                                # remove any fragment anchors
                                if '#' in link:
                                    i = link.rfind('#')
                                    link = link[:i]
                                # if the link is on the same host and is not already crawled,
                                # add it to the crawl list
                                if link.startswith(base) and link not in links_crawled:
                                    links_to_crawl.add(link)
                except:
                    continue
        except:
            msg = "Exception occurred".format()
            self.log.error(msg, exc_info=True)
        finally:
            f = open(output, 'w')
            urls = '\n'.join(links_crawled)
            f.write(urls)
            f.close()

    def run(self):
        """
        Run site check job
        """
        self.configure_logging()
        self.args = self.parser.parse_args()
        self.log.info('Started with ' + ' '.join(sys.argv[1:]))
        # start clock
        start = datetime.datetime.now()
        # execute actions
        if self.args.identify:
            self.identify(self.args.identify, self.args.file1)
        elif self.args.validate:
            # a site URL has not been provided
            self.validate(self.args.validate, self.args.file1)
        elif self.args.validate:
            self.validate(self.args.file1, self.args.file2, self.args.validate)
        # stop clock
        delta = datetime.datetime.now() - start
        s = delta.seconds
        hours, remainder = divmod(s, 3600)
        minutes, seconds = divmod(remainder, 60)
        msg = 'Job finished in %s:%s:%s' % (hours, minutes, seconds)
        self.log.info(msg)

    def validate(self, site, infile, outfile):
        """
        Read the list of URLs from the input file. Where an overriding site URL
        is specified, check the path from the source site against the specified
        alternate site. Report all URLs that fail.
        """
        assert site and infile and outfile, self.log.error("A valid site URL, input and output file must be specified")
        # get the list of urls
        f = open(infile)
        urls = f.readlines()
        f.close()
        # check urls
        new_site = urlparse.urlparse(site)
        with open(outfile, 'w') as out:
            for url in urls:
                if url.endswith('\n'):
                    url = url[:-1]
                old_site = urlparse.urlparse(url)
                target =  "{0}://{1}{2}".format(new_site.scheme, new_site.netloc, old_site.path)
                try:
                    r = requests.get(target)
                    if r.status_code >= 400:
                        raise
                except:
                    out.write("{0}\n".format(target))
                    self.log.error(target)


if __name__ == '__main__':
    checker = SiteChecker()
    checker.run()
