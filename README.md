Site Checker
============

Used to support the migration of web sites. Creates an inventory of URLs for the
specified site. Checks a site to see if a list of URLs are all accessible.
Writes input and output to files to support processing.


Credits
-------

SiteChecker is a project of the eScholarship Research Center at the University
of Melbourne. For more information about the project, please contact us at:

 > eScholarship Research Center
 > University of Melbourne
 > Parkville, Victoria
 > Australia
 > www.esrc.unimelb.edu.au

Authors:

 * Davis Marques <davis.marques@unimelb.edu.au>

Thanks:

 * BeautifulSoup - http://www.crummy.com/software/BeautifulSoup/
 * Python - http://www.python.org
 * requests - http://www.python-requests.org/


License
-------

Please see the LICENSE file for license information.


Installation
------------

Requires Python 2.7.x with BeautifulSoup and requests.


Usage
-----

Run python sitechecker.py -h for usage information.


Revision History
----------------

0.1.0

* First implementation
